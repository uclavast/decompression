#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ap_int.h>
#include <string.h>
#include <ap_utils.h>
#include <hls_stream.h>
using namespace hls;

typedef ap_uint<36> uint36;
typedef ap_uint<34> uint34;
typedef ap_uint<64> uint64;
typedef ap_uint<66> uint66;
typedef ap_uint<68> uint68;
typedef ap_uint<128> uint128;
#define BLOCK_DATA_SIZE 2048
#define BLKSIZE_DEPTH 12
#define WINDOW_SIZE (1 << 15)

void proc_special_lensta(stream<unsigned>& com_lensta, char rtval, ap_uint<32> buflensta)
{
	ap_uint<32> tmp_buflensta;

	tmp_buflensta = buflensta;
	if (rtval == 1) {
		com_lensta.write(tmp_buflensta);;
	}
	else if (rtval == 2) {
		com_lensta.write(0xEFFFFFFF);
	}
	else {
		com_lensta.write(0xFFFFFFFF);
	}
}

char calc_lensta_pipeline(stream<unsigned>& com_lendist, stream<unsigned>& com_lensta, ap_uint<16>* whave_p,
		ap_uint<16>* out_cnt_p, ap_uint<32>* buflensta, unsigned* lendist_cnt_p)
{
	char rtval;
    ap_uint<16> from;
    ap_uint<16> out_cnt;
    ap_uint<16> whave;
    ap_uint<16> tmp_whocnt;
    ap_uint<16> tmp_lenocnt;

    ap_uint<16> winBacksize;
    ap_uint<12> len;
    ap_uint<16> dist;
	ap_uint<4> typeflag;
	ap_uint<12> lenVal_raw;
	ap_uint<12> lenVal_mat;
	ap_uint<16> distVal;
	unsigned tmp_com;
	ap_uint<16> out_sta;
	ap_uint<32> tmp_lensta;
	ap_uint<32> tmp_buflensta;
	ap_uint<32> tmp_winlensta;
	unsigned lendist_cnt; // test

	lendist_cnt = *lendist_cnt_p; // test
	rtval = 0;
	whave = *whave_p;
	out_cnt = *out_cnt_p;
	while (1) {
#pragma HLS PIPELINE II=1
		com_lendist.read(tmp_com);
		lendist_cnt++; // test
		out_sta = 0;
		typeflag = tmp_com >> 28;
		lenVal_raw = (tmp_com >> 24) & 0x0F;
		lenVal_mat = (tmp_com >> 16) & 0x0FFF;
		distVal = tmp_com & 0x0FFFF;
		tmp_whocnt = whave + out_cnt;
		tmp_lenocnt = lenVal_mat + out_cnt;
		if ((typeflag == 0xF) || (typeflag == 0x0)) {
			rtval = 3;
			break;
		}
		else if (typeflag == 8) {
			len = lenVal_mat;
			dist = distVal;
			if (dist > out_cnt) {                /* see if copy from window */
				winBacksize = dist - out_cnt;
				if (dist > tmp_whocnt) {
					rtval = 3;
					break;
				}
				//from = 0;
				from = tmp_whocnt - dist;
				if (dist >= tmp_lenocnt) {
					if (from > out_cnt) {
						if (from - out_cnt >= 32) {
							tmp_winlensta(31, 28) = 2;
						}
						else {
							tmp_buflensta(31, 28) = 4;
						}
					}
					else {
						if (out_cnt - from >= 32) {
							tmp_winlensta(31, 28) = 2;
						}
						else {
							tmp_buflensta(31, 28) = 4;
						}
					}
					//tmp_winlensta(31, 28) = 2;
					tmp_winlensta(27, 16) = len & 0x0FFF;
					tmp_winlensta(15, 0) = from & 0x0FFFF;
					com_lensta.write(tmp_winlensta);
					out_cnt += len;
				}
				else {
					if (from > out_cnt) {
						if (from - out_cnt >= 32) {
							tmp_winlensta(31, 28) = 2;
						}
						else {
							tmp_buflensta(31, 28) = 4;
						}
					}
					else {
						if (out_cnt - from >= 32) {
							tmp_winlensta(31, 28) = 2;
						}
						else {
							tmp_buflensta(31, 28) = 4;
						}
					}
					//tmp_winlensta(31, 28) = 2;
					tmp_winlensta(27, 16) = winBacksize & 0x0FFF;
					tmp_winlensta(15, 0) = from & 0x0FFFF;
					com_lensta.write(tmp_winlensta);
					if (len+32 <= dist) {
						tmp_buflensta(31, 28) = 3;
					}
					else {
						tmp_buflensta(31, 28) = 4;
					}
					tmp_buflensta(27, 16) = (len - winBacksize) & 0x0FFF;
					tmp_buflensta(15, 0) = 0;
					rtval = 1;
					out_cnt += len;
					break;
				}
			}
			else {
				out_sta = out_cnt - dist;          /* copy direct from output */
				if (len+20 < dist) {
					tmp_buflensta(31, 28) = 3;
				}
				else {
					tmp_buflensta(31, 28) = 4;
				}
				tmp_buflensta(27, 16) = len & 0x0FFF;
				tmp_buflensta(15, 0) = out_sta;
				com_lensta.write(tmp_buflensta);
				out_cnt += len;
			}
		}
		else {
			len = lenVal_raw;
			tmp_lensta = tmp_com;
			com_lensta.write(tmp_lensta);
			out_cnt += len;
		}
		//if (out_cnt >= 32768) {
		if (out_cnt >= WINDOW_SIZE) {
			whave = out_cnt;
			out_cnt = 0;
			rtval = 2;
			break;
		}
	}
	*whave_p = whave;
	*out_cnt_p = out_cnt;
	*buflensta = tmp_buflensta;
	*lendist_cnt_p = lendist_cnt; // test
	return rtval;
}

void calc_lensta(stream<unsigned>& com_lendist, stream<unsigned>& com_lensta)
{
	char rtval;
    ap_uint<16> out_cnt;
    ap_uint<16> whave;
    ap_uint<32> buflensta;
    unsigned lendist_cnt; // test

    lendist_cnt = 0; // test
    out_cnt = 0;
	whave = 0;
	buflensta = 0;

	while (1) {
		rtval = calc_lensta_pipeline(com_lendist, com_lensta, &whave, &out_cnt, &buflensta, &lendist_cnt);
		proc_special_lensta(com_lensta, rtval, buflensta);
		if (rtval == 3) {
			break;
		}
	}
	printf("lendist_cnt = %d\n", lendist_cnt);
}

char move_match_pipeline(stream<unsigned>& com_lensta, uint64* decodeBuf,
		stream<uint68>& decout, ap_uint<32>* tmp_lensta_p, ap_uint<16>* out_int_i_p,
		ap_uint<128>* hold_p, ap_uint<4>* bytes_p, unsigned* out_p)
{
    unsigned out;
    char rtval;
    ap_uint<16> out_int_i;
    unsigned i;
	ap_uint<4> type_cpy;
	ap_uint<32> tmp_lensta;
	ap_uint<128> hold;
	ap_uint<4> bytes;
	ap_uint<4> partbytes;
	ap_uint<4> bytenum;
	ap_uint<4> rsl_num;
	ap_uint<9> Len2copy;
	ap_uint<16> Sta2copy;
	bool fircpy_flag;
	ap_uint<64> tmp_decode;
	ap_uint<16> raddr;
	ap_uint<64> tmpdec2cpy;
	ap_uint<4> tmpBnum2cpy;
	uint68 dec2assem;

	out_int_i = *out_int_i_p;
	out = *out_p;
	hold = *hold_p;
	bytes = *bytes_p;
	rtval = 0;
	tmp_lensta = 0;
	i = 0;

    bool inner = false;
	while(1) {
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=10 avg=10
#pragma HLS DEPENDENCE variable=decodeBuf array inter false
#pragma HLS DEPENDENCE variable=decodeBuf array intra false
		//tmp_lensta = com_lensta[i];
//		if (!com_lensta.empty()) {
		if (!inner) {
			tmp_lensta = com_lensta.read();
			type_cpy = tmp_lensta >> 28;
			fircpy_flag = true;
			Len2copy = tmp_lensta(24, 16); //(tmp_lensta >> 16) & 0x0FFF;
			Sta2copy = tmp_lensta(15, 0);// & 0x0FFFF;
			rsl_num = tmp_lensta(2, 0);
			bytenum = 8 - rsl_num;
			raddr = tmp_lensta(15, 3); //Sta2copy >> 2;
			//i++;
			//if (i == 4288) {
			//	printf("Test.\n");
			//}
			if (type_cpy == 0xE) {
				rtval = 1;
				break;
			}
			else if (type_cpy == 0xF) {
				rtval = 2;
				break;
			}
			//else if ((type_cpy == 0x2) || (type_cpy == 0x3)) {
			//	tmp_windat = windowdat[raddr] >> rsl_num*8;
			//	tmp_decdat = decodeBuf[raddr] >> rsl_num*8;
			//	raddr++;
			//}
			else if (type_cpy == 0x4){
				rtval = 4;
				break;
			}
			inner = true;
		}

		if (type_cpy == 0x1) {
			tmpBnum2cpy = tmp_lensta(27, 24);
			tmpdec2cpy(63, 24) = 0;
			tmpdec2cpy(23, 0) = tmp_lensta(23, 0);
			//Len2copy = 0;
			inner = false;
		}
		else { //if ((type_cpy == 0x2) || (type_cpy == 0x3)) {
			//if (type_cpy == 0x2) {
			//	tmp_decode = windowdat[raddr++] >> rsl_num*8;
			//}
			//else {
				tmp_decode = decodeBuf[raddr++] >> rsl_num*8;
			//}
			if (fircpy_flag == true) {
				if (bytenum >= Len2copy) {
					tmpdec2cpy = tmp_decode; //(Len2copy*8-1, 0);
					tmpBnum2cpy = Len2copy;
					Len2copy = 0;
					inner = false;
				}
				else {
					tmpdec2cpy = tmp_decode; //(bytenum*8-1, 0);
					tmpBnum2cpy = bytenum;
					Len2copy -= bytenum;
				}
			}
			else {
				if (Len2copy > 8) {
					tmpdec2cpy = tmp_decode;
					tmpBnum2cpy = bytenum;
					Len2copy -= bytenum;
				}
				else {
					tmpdec2cpy = tmp_decode; //(Len2copy*8-1, 0);
					tmpBnum2cpy = Len2copy;
					Len2copy = 0;
					inner = false;
				}
			}
			rsl_num = 0;
			bytenum = 8;
			fircpy_flag = false;
			//tmp_windat = windowdat[raddr];// >> rsl_num*8;
			//tmp_decdat = decodeBuf[raddr];// >> rsl_num*8;
			//raddr++;
		}
		hold(bytes*8+63, bytes*8) = tmpdec2cpy;
		bytes += tmpBnum2cpy;
		out += tmpBnum2cpy;
		if (bytes >= 8) {
			decodeBuf[out_int_i] = hold(63, 0);
			out_int_i++;
			hold >>= 64;
			bytes -= 8;
		}
		dec2assem(67, 64) = tmpBnum2cpy;
		dec2assem(63, 0) = tmpdec2cpy;
		//if (!decout.full()) {
			decout.write(dec2assem);
		//}
	}
	*out_int_i_p = out_int_i;
	*out_p = out;
	*hold_p = hold;
	*bytes_p = bytes;
	*tmp_lensta_p = tmp_lensta;
	return rtval;
}

void move_match_special(stream<unsigned>& com_lensta, uint64* decodeBuf, 
		stream<uint68>& decout, ap_uint<32> tmp_lensta, ap_uint<16>* out_int_i_p,
		ap_uint<128>* hold_p, ap_uint<4>* bytes_p, unsigned* out_p)
{
    unsigned out;
    //char rtval;
    ap_uint<16> out_int_i;
    //unsigned dout_i;
	//ap_uint<4> type_cpy;
	//ap_uint<32> tmp_lensta;
	ap_uint<128> hold;
	ap_uint<64> parthold;
	ap_uint<4> bytes;
	ap_uint<4> partbytes;
	ap_uint<4> bytenum;
	ap_uint<4> rsl_num;
	ap_uint<16> Len2copy;
	ap_uint<16> Sta2copy;
	ap_uint<2> fircpy_flag;
	ap_uint<64> tmp_decdat;
	ap_uint<64> tmp_decode;
	ap_uint<16> raddr;
	ap_uint<64> tmpdec2cpy;
	ap_uint<4> tmpBnum2cpy;
	uint68 dec2assem;

	out_int_i = *out_int_i_p;
	out = *out_p;
	hold = *hold_p;
	bytes = *bytes_p;
	//rtval = 0;

	fircpy_flag = 0;
	Len2copy = tmp_lensta(27, 16); //(tmp_lensta >> 16) & 0x0FFF;
	Sta2copy = tmp_lensta(15, 0);// & 0x0FFFF;
	rsl_num = tmp_lensta(2, 0);
	bytenum = 8 - rsl_num;
	raddr = Sta2copy >> 3;
	while(1) {
#pragma HLS PIPELINE II=4
#pragma HLS LOOP_TRIPCOUNT min=10 avg=10
		tmp_decdat = decodeBuf[raddr] >> rsl_num*8;
		if (raddr == out_int_i) {
			parthold = (ap_uint<64>)hold >> rsl_num*8;
			partbytes = bytes - rsl_num;
			if (partbytes == 1) {
				//tmp_decode = (parthold(7, 0) << 24) + (parthold(7, 0) << 16) + (parthold(7, 0) << 8) + parthold(7, 0);
				tmp_decode(63, 56) = parthold(7, 0);
				tmp_decode(55, 48) = parthold(7, 0);
				tmp_decode(47, 40) = parthold(7, 0);
				tmp_decode(39, 32) = parthold(7, 0);
				tmp_decode(31, 24) = parthold(7, 0);
				tmp_decode(23, 16) = parthold(7, 0);
				tmp_decode(15, 8) = parthold(7, 0);
				tmp_decode(7, 0) = parthold(7, 0);
			}
			else if (partbytes == 2) {
				//tmp_decode = (parthold(15, 0) << 16) + parthold(15, 0);
				tmp_decode(63, 48) = parthold(15, 0);
				tmp_decode(47, 32) = parthold(15, 0);
				tmp_decode(31, 16) = parthold(15, 0);
				tmp_decode(15, 0) = parthold(15, 0);
			}
			else if (partbytes == 3) {
				//tmp_decode = (parthold(15, 0) << 16) + parthold(15, 0);
				tmp_decode(63, 48) = parthold(15, 0);
				tmp_decode(47, 24) = parthold(23, 0);
				tmp_decode(23, 0) = parthold(23, 0);
			}
			else if (partbytes == 4) {
				tmp_decode(63, 32) = parthold(31, 0);
				tmp_decode(31, 0) = parthold(31, 0);
			}
			else if (partbytes == 5) {
				tmp_decode(63, 40) = parthold(23, 0);
				tmp_decode(39, 0) = parthold(39, 0);
			}
			else if (partbytes == 6) {
				tmp_decode(63, 48) = parthold(15, 0);
				tmp_decode(47, 0) = parthold(47, 0);
			}
			else if (partbytes == 7) {
				tmp_decode(63, 56) = parthold(7, 0);
				tmp_decode(55, 0) = parthold(55, 0);
			}
			else {
				tmp_decode = parthold;
			}
		}
		else {
			//tmp_decode = decodeBuf[raddr] >> rsl_num*8;
			tmp_decode = tmp_decdat;
		}
		if (fircpy_flag == 0) {
			if (bytenum >= Len2copy) {
				tmpdec2cpy = tmp_decode; //(Len2copy*8-1, 0);
				tmpBnum2cpy = Len2copy;
				Len2copy = 0;
			}
			else {
				tmpdec2cpy = tmp_decode; //(bytenum*8-1, 0);
				tmpBnum2cpy = bytenum;
				Len2copy -= bytenum;
			}
		}
		else {
			if (Len2copy > 8) {
				tmpdec2cpy = tmp_decode;
				tmpBnum2cpy = bytenum;
				Len2copy -= bytenum;
			}
			else {
				tmpdec2cpy = tmp_decode; //(Len2copy*8-1, 0);
				tmpBnum2cpy = Len2copy;
				Len2copy = 0;
			}
		}
		hold(bytes*8+63, bytes*8) = tmpdec2cpy;
		bytes += tmpBnum2cpy;
		out += tmpBnum2cpy;
		if (bytes >= 8) {
			decodeBuf[out_int_i] = hold(63, 0);
			out_int_i++;
			hold >>= 64;
			bytes -= 8;
		}
		dec2assem(67, 64) = tmpBnum2cpy;
		dec2assem(63, 0) = tmpdec2cpy;
		//if (!decout.full()) {
			decout.write(dec2assem);
		//}
		if (Len2copy == 0) {
			break;
		}
		raddr++;
		rsl_num = 0;
		bytenum = 8;
		fircpy_flag = 1;
	}
	*out_int_i_p = out_int_i;
	*out_p = out;
	*hold_p = hold;
	*bytes_p = bytes;
	//return rtval;
}

char copy_data(stream<unsigned>& com_lensta, uint64* decodeBuf, 
		stream<uint68>& decout, unsigned* out_p)
{
    unsigned out;     /* local strm->next_out */
    ap_uint<16> out_int_i;
    //unsigned dout_i;
	ap_uint<32> tmp_lensta;
	ap_uint<128> hold;
	ap_uint<4> bytes;
	char rtval;
	uint68 dec2assem;

	out = *out_p;
	rtval = 0;
	hold = 0;
	bytes = 0;
	out_int_i = 0;
	dec2assem = 0;
	tmp_lensta = 0;
	while (1) {
		rtval = move_match_pipeline(com_lensta, decodeBuf, 
				decout, &tmp_lensta, &out_int_i, &hold, &bytes, &out);
		if (rtval == 4) {
			move_match_special(com_lensta, decodeBuf, 
					decout, tmp_lensta, &out_int_i, &hold, &bytes, &out);
		}
		else if (rtval == 1) {
			if (bytes != 0) {
				decodeBuf[out_int_i] = hold(63, 0);
				out_int_i++;
			}
			break;
		}
		else if (rtval == 2) {
			//if (!decout.full()) {
				if (bytes != 0) {
					dec2assem(67, 64) = bytes;
					dec2assem(63, 0) = hold(63, 0);
					decout.write(dec2assem);
				}
				dec2assem(67, 64) = 0xF;
				dec2assem(63, 0) = out;
				decout.write(dec2assem);
			//}
			//if (!decout.full()) {
				//decout.write(0xFFFFFFFFF);
			//}
			break;
		}
	}
	*out_p = out;
	return rtval;
}


void inflate_decoder(unsigned* bramcodein, stream<unsigned>& com_lendist, ap_uint<2> blkIdx, //ap_uint<16> blkSize,
		ap_uint<32>* hold_p, ap_uint<96>* long_hold_p, ap_uint<7>* bits_p)
{
//    struct inflate_state FAR *state;

	ap_uint<4> lencode1_extbits[16] = {
			0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x0, 0x2,
			0x0, 0x1, 0x0, 0x2,	0x0, 0x1, 0x0, 0x2
	};
#pragma HLS ARRAY_PARTITION variable=lencode1_extbits complete dim=1
	ap_uint<4> lencode2_extbits[8] = {
			0x4, 0x5, 0x5, 0x0, 0x5, 0x0, 0x5, 0x0
	};
#pragma HLS ARRAY_PARTITION variable=lencode2_extbits complete dim=1
	ap_uint<4> lencode3_extbits[8] = {
			0x2, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4
	};
#pragma HLS ARRAY_PARTITION variable=lencode3_extbits complete dim=1
	ap_uint<4> lencode1_bits[32] = {
			0x7, 0x8, 0x8, 0x8, 0x7, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x8, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x7, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x8, 0x8, 0x8, 0x9
	};
#pragma HLS ARRAY_PARTITION variable=lencode1_bits complete dim=1
	ap_uint<4> lencode2_bits[32] = {
			0x7, 0x8, 0x8, 0x8, 0x7, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x8, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x7, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x8, 0x8, 0x8, 0x9
	};
#pragma HLS ARRAY_PARTITION variable=lencode2_bits complete dim=1
	ap_uint<4> lencode3_bits[32] = {
			0x7, 0x8, 0x8, 0x8, 0x7, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x8, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x7, 0x8, 0x8, 0x9,
			0x7, 0x8, 0x8, 0x9, 0x8, 0x8, 0x8, 0x9
	};
#pragma HLS ARRAY_PARTITION variable=lencode3_bits complete dim=1
	unsigned short lencode1_val[512] = {
			000000, 0x0050, 0x0010, 0x0073, 0x001f, 0x0070, 0x0030, 0x00c0,
			0x000a, 0x0060, 0x0020, 0x00a0, 000000, 0x0080, 0x0040, 0x00e0,
			0x0006, 0x0058, 0x0018, 0x0090, 0x003b, 0x0078, 0x0038, 0x00d0,
			0x0011, 0x0068, 0x0028, 0x00b0, 0x0008, 0x0088, 0x0048, 0x00f0,
			0x0004, 0x0054, 0x0014, 0x00e3, 0x002b, 0x0074, 0x0034, 0x00c8,
			0x000d, 0x0064, 0x0024, 0x00a8, 0x0004, 0x0084, 0x0044, 0x00e8,
			0x0008, 0x005c, 0x001c, 0x0098, 0x0053, 0x007c, 0x003c, 0x00d8,
			0x0017, 0x006c, 0x002c, 0x00b8, 0x000c, 0x008c, 0x004c, 0x00f8,
			0x0003, 0x0052, 0x0012, 0x00a3, 0x0023, 0x0072, 0x0032, 0x00c4,
			0x000b, 0x0062, 0x0022, 0x00a4, 0x0002, 0x0082, 0x0042, 0x00e4,
			0x0007, 0x005a, 0x001a, 0x0094, 0x0043, 0x007a, 0x003a, 0x00d4,
			0x0013, 0x006a, 0x002a, 0x00b4, 0x000a, 0x008a, 0x004a, 0x00f4,
			0x0005, 0x0056, 0x0016, 000000, 0x0033, 0x0076, 0x0036, 0x00cc,
			0x000f, 0x0066, 0x0026, 0x00ac, 0x0006, 0x0086, 0x0046, 0x00ec,
			0x0009, 0x005e, 0x001e, 0x009c, 0x0063, 0x007e, 0x003e, 0x00dc,
			0x001b, 0x006e, 0x002e, 0x00bc, 0x000e, 0x008e, 0x004e, 0x00fc,
			000000, 0x0051, 0x0011, 0x0083, 0x001f, 0x0071, 0x0031, 0x00c2,
			0x000a, 0x0061, 0x0021, 0x00a2, 0x0001, 0x0081, 0x0041, 0x00e2,
			0x0006, 0x0059, 0x0019, 0x0092, 0x003b, 0x0079, 0x0039, 0x00d2,
			0x0011, 0x0069, 0x0029, 0x00b2, 0x0009, 0x0089, 0x0049, 0x00f2,
			0x0004, 0x0055, 0x0015, 0x0102, 0x002b, 0x0075, 0x0035, 0x00ca,
			0x000d, 0x0065, 0x0025, 0x00aa, 0x0005, 0x0085, 0x0045, 0x00ea,
			0x0008, 0x005d, 0x001d, 0x009a, 0x0053, 0x007d, 0x003d, 0x00da,
			0x0017, 0x006d, 0x002d, 0x00ba, 0x000d, 0x008d, 0x004d, 0x00fa,
			0x0003, 0x0053, 0x0013, 0x00c3, 0x0023, 0x0073, 0x0033, 0x00c6,
			0x000b, 0x0063, 0x0023, 0x00a6, 0x0003, 0x0083, 0x0043, 0x00e6,
			0x0007, 0x005b, 0x001b, 0x0096, 0x0043, 0x007b, 0x003b, 0x00d6,
			0x0013, 0x006b, 0x002b, 0x00b6, 0x000b, 0x008b, 0x004b, 0x00f6,
			0x0005, 0x0057, 0x0017, 000000, 0x0033, 0x0077, 0x0037, 0x00ce,
			0x000f, 0x0067, 0x0027, 0x00ae, 0x0007, 0x0087, 0x0047, 0x00ee,
			0x0009, 0x005f, 0x001f, 0x009e, 0x0063, 0x007f, 0x003f, 0x00de,
			0x001b, 0x006f, 0x002f, 0x00be, 0x000f, 0x008f, 0x004f, 0x00fe,
			000000, 0x0050, 0x0010, 0x0073, 0x001f, 0x0070, 0x0030, 0x00c1,
			0x000a, 0x0060, 0x0020, 0x00a1, 000000, 0x0080, 0x0040, 0x00e1,
			0x0006, 0x0058, 0x0018, 0x0091, 0x003b, 0x0078, 0x0038, 0x00d1,
			0x0011, 0x0068, 0x0028, 0x00b1, 0x0008, 0x0088, 0x0048, 0x00f1,
			0x0004, 0x0054, 0x0014, 0x00e3, 0x002b, 0x0074, 0x0034, 0x00c9,
			0x000d, 0x0064, 0x0024, 0x00a9, 0x0004, 0x0084, 0x0044, 0x00e9,
			0x0008, 0x005c, 0x001c, 0x0099, 0x0053, 0x007c, 0x003c, 0x00d9,
			0x0017, 0x006c, 0x002c, 0x00b9, 0x000c, 0x008c, 0x004c, 0x00f9,
			0x0003, 0x0052, 0x0012, 0x00a3, 0x0023, 0x0072, 0x0032, 0x00c5,
			0x000b, 0x0062, 0x0022, 0x00a5, 0x0002, 0x0082, 0x0042, 0x00e5,
			0x0007, 0x005a, 0x001a, 0x0095, 0x0043, 0x007a, 0x003a, 0x00d5,
			0x0013, 0x006a, 0x002a, 0x00b5, 0x000a, 0x008a, 0x004a, 0x00f5,
			0x0005, 0x0056, 0x0016, 000000, 0x0033, 0x0076, 0x0036, 0x00cd,
			0x000f, 0x0066, 0x0026, 0x00ad, 0x0006, 0x0086, 0x0046, 0x00ed,
			0x0009, 0x005e, 0x001e, 0x009d, 0x0063, 0x007e, 0x003e, 0x00dd,
			0x001b, 0x006e, 0x002e, 0x00bd, 0x000e, 0x008e, 0x004e, 0x00fd,
			000000, 0x0051, 0x0011, 0x0083, 0x001f, 0x0071, 0x0031, 0x00c3,
			0x000a, 0x0061, 0x0021, 0x00a3, 0x0001, 0x0081, 0x0041, 0x00e3,
			0x0006, 0x0059, 0x0019, 0x0093, 0x003b, 0x0079, 0x0039, 0x00d3,
			0x0011, 0x0069, 0x0029, 0x00b3, 0x0009, 0x0089, 0x0049, 0x00f3,
			0x0004, 0x0055, 0x0015, 0x0102, 0x002b, 0x0075, 0x0035, 0x00cb,
			0x000d, 0x0065, 0x0025, 0x00ab, 0x0005, 0x0085, 0x0045, 0x00eb,
			0x0008, 0x005d, 0x001d, 0x009b, 0x0053, 0x007d, 0x003d, 0x00db,
			0x0017, 0x006d, 0x002d, 0x00bb, 0x000d, 0x008d, 0x004d, 0x00fb,
			0x0003, 0x0053, 0x0013, 0x00c3, 0x0023, 0x0073, 0x0033, 0x00c7,
			0x000b, 0x0063, 0x0023, 0x00a7, 0x0003, 0x0083, 0x0043, 0x00e7,
			0x0007, 0x005b, 0x001b, 0x0097, 0x0043, 0x007b, 0x003b, 0x00d7,
			0x0013, 0x006b, 0x002b, 0x00b7, 0x000b, 0x008b, 0x004b, 0x00f7,
			0x0005, 0x0057, 0x0017, 000000, 0x0033, 0x0077, 0x0037, 0x00cf,
			0x000f, 0x0067, 0x0027, 0x00af, 0x0007, 0x0087, 0x0047, 0x00ef,
			0x0009, 0x005f, 0x001f, 0x009f, 0x0063, 0x007f, 0x003f, 0x00df,
			0x001b, 0x006f, 0x002f, 0x00bf, 0x000f, 0x008f, 0x004f, 0x00ff
	};
	unsigned short lencode2_val[512] = {
			000000, 0x0050, 0x0010, 0x0073, 0x001f, 0x0070, 0x0030, 0x00c0,
			0x000a, 0x0060, 0x0020, 0x00a0, 000000, 0x0080, 0x0040, 0x00e0,
			0x0006, 0x0058, 0x0018, 0x0090, 0x003b, 0x0078, 0x0038, 0x00d0,
			0x0011, 0x0068, 0x0028, 0x00b0, 0x0008, 0x0088, 0x0048, 0x00f0,
			0x0004, 0x0054, 0x0014, 0x00e3, 0x002b, 0x0074, 0x0034, 0x00c8,
			0x000d, 0x0064, 0x0024, 0x00a8, 0x0004, 0x0084, 0x0044, 0x00e8,
			0x0008, 0x005c, 0x001c, 0x0098, 0x0053, 0x007c, 0x003c, 0x00d8,
			0x0017, 0x006c, 0x002c, 0x00b8, 0x000c, 0x008c, 0x004c, 0x00f8,
			0x0003, 0x0052, 0x0012, 0x00a3, 0x0023, 0x0072, 0x0032, 0x00c4,
			0x000b, 0x0062, 0x0022, 0x00a4, 0x0002, 0x0082, 0x0042, 0x00e4,
			0x0007, 0x005a, 0x001a, 0x0094, 0x0043, 0x007a, 0x003a, 0x00d4,
			0x0013, 0x006a, 0x002a, 0x00b4, 0x000a, 0x008a, 0x004a, 0x00f4,
			0x0005, 0x0056, 0x0016, 000000, 0x0033, 0x0076, 0x0036, 0x00cc,
			0x000f, 0x0066, 0x0026, 0x00ac, 0x0006, 0x0086, 0x0046, 0x00ec,
			0x0009, 0x005e, 0x001e, 0x009c, 0x0063, 0x007e, 0x003e, 0x00dc,
			0x001b, 0x006e, 0x002e, 0x00bc, 0x000e, 0x008e, 0x004e, 0x00fc,
			000000, 0x0051, 0x0011, 0x0083, 0x001f, 0x0071, 0x0031, 0x00c2,
			0x000a, 0x0061, 0x0021, 0x00a2, 0x0001, 0x0081, 0x0041, 0x00e2,
			0x0006, 0x0059, 0x0019, 0x0092, 0x003b, 0x0079, 0x0039, 0x00d2,
			0x0011, 0x0069, 0x0029, 0x00b2, 0x0009, 0x0089, 0x0049, 0x00f2,
			0x0004, 0x0055, 0x0015, 0x0102, 0x002b, 0x0075, 0x0035, 0x00ca,
			0x000d, 0x0065, 0x0025, 0x00aa, 0x0005, 0x0085, 0x0045, 0x00ea,
			0x0008, 0x005d, 0x001d, 0x009a, 0x0053, 0x007d, 0x003d, 0x00da,
			0x0017, 0x006d, 0x002d, 0x00ba, 0x000d, 0x008d, 0x004d, 0x00fa,
			0x0003, 0x0053, 0x0013, 0x00c3, 0x0023, 0x0073, 0x0033, 0x00c6,
			0x000b, 0x0063, 0x0023, 0x00a6, 0x0003, 0x0083, 0x0043, 0x00e6,
			0x0007, 0x005b, 0x001b, 0x0096, 0x0043, 0x007b, 0x003b, 0x00d6,
			0x0013, 0x006b, 0x002b, 0x00b6, 0x000b, 0x008b, 0x004b, 0x00f6,
			0x0005, 0x0057, 0x0017, 000000, 0x0033, 0x0077, 0x0037, 0x00ce,
			0x000f, 0x0067, 0x0027, 0x00ae, 0x0007, 0x0087, 0x0047, 0x00ee,
			0x0009, 0x005f, 0x001f, 0x009e, 0x0063, 0x007f, 0x003f, 0x00de,
			0x001b, 0x006f, 0x002f, 0x00be, 0x000f, 0x008f, 0x004f, 0x00fe,
			000000, 0x0050, 0x0010, 0x0073, 0x001f, 0x0070, 0x0030, 0x00c1,
			0x000a, 0x0060, 0x0020, 0x00a1, 000000, 0x0080, 0x0040, 0x00e1,
			0x0006, 0x0058, 0x0018, 0x0091, 0x003b, 0x0078, 0x0038, 0x00d1,
			0x0011, 0x0068, 0x0028, 0x00b1, 0x0008, 0x0088, 0x0048, 0x00f1,
			0x0004, 0x0054, 0x0014, 0x00e3, 0x002b, 0x0074, 0x0034, 0x00c9,
			0x000d, 0x0064, 0x0024, 0x00a9, 0x0004, 0x0084, 0x0044, 0x00e9,
			0x0008, 0x005c, 0x001c, 0x0099, 0x0053, 0x007c, 0x003c, 0x00d9,
			0x0017, 0x006c, 0x002c, 0x00b9, 0x000c, 0x008c, 0x004c, 0x00f9,
			0x0003, 0x0052, 0x0012, 0x00a3, 0x0023, 0x0072, 0x0032, 0x00c5,
			0x000b, 0x0062, 0x0022, 0x00a5, 0x0002, 0x0082, 0x0042, 0x00e5,
			0x0007, 0x005a, 0x001a, 0x0095, 0x0043, 0x007a, 0x003a, 0x00d5,
			0x0013, 0x006a, 0x002a, 0x00b5, 0x000a, 0x008a, 0x004a, 0x00f5,
			0x0005, 0x0056, 0x0016, 000000, 0x0033, 0x0076, 0x0036, 0x00cd,
			0x000f, 0x0066, 0x0026, 0x00ad, 0x0006, 0x0086, 0x0046, 0x00ed,
			0x0009, 0x005e, 0x001e, 0x009d, 0x0063, 0x007e, 0x003e, 0x00dd,
			0x001b, 0x006e, 0x002e, 0x00bd, 0x000e, 0x008e, 0x004e, 0x00fd,
			000000, 0x0051, 0x0011, 0x0083, 0x001f, 0x0071, 0x0031, 0x00c3,
			0x000a, 0x0061, 0x0021, 0x00a3, 0x0001, 0x0081, 0x0041, 0x00e3,
			0x0006, 0x0059, 0x0019, 0x0093, 0x003b, 0x0079, 0x0039, 0x00d3,
			0x0011, 0x0069, 0x0029, 0x00b3, 0x0009, 0x0089, 0x0049, 0x00f3,
			0x0004, 0x0055, 0x0015, 0x0102, 0x002b, 0x0075, 0x0035, 0x00cb,
			0x000d, 0x0065, 0x0025, 0x00ab, 0x0005, 0x0085, 0x0045, 0x00eb,
			0x0008, 0x005d, 0x001d, 0x009b, 0x0053, 0x007d, 0x003d, 0x00db,
			0x0017, 0x006d, 0x002d, 0x00bb, 0x000d, 0x008d, 0x004d, 0x00fb,
			0x0003, 0x0053, 0x0013, 0x00c3, 0x0023, 0x0073, 0x0033, 0x00c7,
			0x000b, 0x0063, 0x0023, 0x00a7, 0x0003, 0x0083, 0x0043, 0x00e7,
			0x0007, 0x005b, 0x001b, 0x0097, 0x0043, 0x007b, 0x003b, 0x00d7,
			0x0013, 0x006b, 0x002b, 0x00b7, 0x000b, 0x008b, 0x004b, 0x00f7,
			0x0005, 0x0057, 0x0017, 000000, 0x0033, 0x0077, 0x0037, 0x00cf,
			0x000f, 0x0067, 0x0027, 0x00af, 0x0007, 0x0087, 0x0047, 0x00ef,
			0x0009, 0x005f, 0x001f, 0x009f, 0x0063, 0x007f, 0x003f, 0x00df,
			0x001b, 0x006f, 0x002f, 0x00bf, 0x000f, 0x008f, 0x004f, 0x00ff
	};

	unsigned short lencode3_val[512] = {
			000000, 0x0050, 0x0010, 0x0073, 0x001f, 0x0070, 0x0030, 0x00c0,
			0x000a, 0x0060, 0x0020, 0x00a0, 000000, 0x0080, 0x0040, 0x00e0,
			0x0006, 0x0058, 0x0018, 0x0090, 0x003b, 0x0078, 0x0038, 0x00d0,
			0x0011, 0x0068, 0x0028, 0x00b0, 0x0008, 0x0088, 0x0048, 0x00f0,
			0x0004, 0x0054, 0x0014, 0x00e3, 0x002b, 0x0074, 0x0034, 0x00c8,
			0x000d, 0x0064, 0x0024, 0x00a8, 0x0004, 0x0084, 0x0044, 0x00e8,
			0x0008, 0x005c, 0x001c, 0x0098, 0x0053, 0x007c, 0x003c, 0x00d8,
			0x0017, 0x006c, 0x002c, 0x00b8, 0x000c, 0x008c, 0x004c, 0x00f8,
			0x0003, 0x0052, 0x0012, 0x00a3, 0x0023, 0x0072, 0x0032, 0x00c4,
			0x000b, 0x0062, 0x0022, 0x00a4, 0x0002, 0x0082, 0x0042, 0x00e4,
			0x0007, 0x005a, 0x001a, 0x0094, 0x0043, 0x007a, 0x003a, 0x00d4,
			0x0013, 0x006a, 0x002a, 0x00b4, 0x000a, 0x008a, 0x004a, 0x00f4,
			0x0005, 0x0056, 0x0016, 000000, 0x0033, 0x0076, 0x0036, 0x00cc,
			0x000f, 0x0066, 0x0026, 0x00ac, 0x0006, 0x0086, 0x0046, 0x00ec,
			0x0009, 0x005e, 0x001e, 0x009c, 0x0063, 0x007e, 0x003e, 0x00dc,
			0x001b, 0x006e, 0x002e, 0x00bc, 0x000e, 0x008e, 0x004e, 0x00fc,
			000000, 0x0051, 0x0011, 0x0083, 0x001f, 0x0071, 0x0031, 0x00c2,
			0x000a, 0x0061, 0x0021, 0x00a2, 0x0001, 0x0081, 0x0041, 0x00e2,
			0x0006, 0x0059, 0x0019, 0x0092, 0x003b, 0x0079, 0x0039, 0x00d2,
			0x0011, 0x0069, 0x0029, 0x00b2, 0x0009, 0x0089, 0x0049, 0x00f2,
			0x0004, 0x0055, 0x0015, 0x0102, 0x002b, 0x0075, 0x0035, 0x00ca,
			0x000d, 0x0065, 0x0025, 0x00aa, 0x0005, 0x0085, 0x0045, 0x00ea,
			0x0008, 0x005d, 0x001d, 0x009a, 0x0053, 0x007d, 0x003d, 0x00da,
			0x0017, 0x006d, 0x002d, 0x00ba, 0x000d, 0x008d, 0x004d, 0x00fa,
			0x0003, 0x0053, 0x0013, 0x00c3, 0x0023, 0x0073, 0x0033, 0x00c6,
			0x000b, 0x0063, 0x0023, 0x00a6, 0x0003, 0x0083, 0x0043, 0x00e6,
			0x0007, 0x005b, 0x001b, 0x0096, 0x0043, 0x007b, 0x003b, 0x00d6,
			0x0013, 0x006b, 0x002b, 0x00b6, 0x000b, 0x008b, 0x004b, 0x00f6,
			0x0005, 0x0057, 0x0017, 000000, 0x0033, 0x0077, 0x0037, 0x00ce,
			0x000f, 0x0067, 0x0027, 0x00ae, 0x0007, 0x0087, 0x0047, 0x00ee,
			0x0009, 0x005f, 0x001f, 0x009e, 0x0063, 0x007f, 0x003f, 0x00de,
			0x001b, 0x006f, 0x002f, 0x00be, 0x000f, 0x008f, 0x004f, 0x00fe,
			000000, 0x0050, 0x0010, 0x0073, 0x001f, 0x0070, 0x0030, 0x00c1,
			0x000a, 0x0060, 0x0020, 0x00a1, 000000, 0x0080, 0x0040, 0x00e1,
			0x0006, 0x0058, 0x0018, 0x0091, 0x003b, 0x0078, 0x0038, 0x00d1,
			0x0011, 0x0068, 0x0028, 0x00b1, 0x0008, 0x0088, 0x0048, 0x00f1,
			0x0004, 0x0054, 0x0014, 0x00e3, 0x002b, 0x0074, 0x0034, 0x00c9,
			0x000d, 0x0064, 0x0024, 0x00a9, 0x0004, 0x0084, 0x0044, 0x00e9,
			0x0008, 0x005c, 0x001c, 0x0099, 0x0053, 0x007c, 0x003c, 0x00d9,
			0x0017, 0x006c, 0x002c, 0x00b9, 0x000c, 0x008c, 0x004c, 0x00f9,
			0x0003, 0x0052, 0x0012, 0x00a3, 0x0023, 0x0072, 0x0032, 0x00c5,
			0x000b, 0x0062, 0x0022, 0x00a5, 0x0002, 0x0082, 0x0042, 0x00e5,
			0x0007, 0x005a, 0x001a, 0x0095, 0x0043, 0x007a, 0x003a, 0x00d5,
			0x0013, 0x006a, 0x002a, 0x00b5, 0x000a, 0x008a, 0x004a, 0x00f5,
			0x0005, 0x0056, 0x0016, 000000, 0x0033, 0x0076, 0x0036, 0x00cd,
			0x000f, 0x0066, 0x0026, 0x00ad, 0x0006, 0x0086, 0x0046, 0x00ed,
			0x0009, 0x005e, 0x001e, 0x009d, 0x0063, 0x007e, 0x003e, 0x00dd,
			0x001b, 0x006e, 0x002e, 0x00bd, 0x000e, 0x008e, 0x004e, 0x00fd,
			000000, 0x0051, 0x0011, 0x0083, 0x001f, 0x0071, 0x0031, 0x00c3,
			0x000a, 0x0061, 0x0021, 0x00a3, 0x0001, 0x0081, 0x0041, 0x00e3,
			0x0006, 0x0059, 0x0019, 0x0093, 0x003b, 0x0079, 0x0039, 0x00d3,
			0x0011, 0x0069, 0x0029, 0x00b3, 0x0009, 0x0089, 0x0049, 0x00f3,
			0x0004, 0x0055, 0x0015, 0x0102, 0x002b, 0x0075, 0x0035, 0x00cb,
			0x000d, 0x0065, 0x0025, 0x00ab, 0x0005, 0x0085, 0x0045, 0x00eb,
			0x0008, 0x005d, 0x001d, 0x009b, 0x0053, 0x007d, 0x003d, 0x00db,
			0x0017, 0x006d, 0x002d, 0x00bb, 0x000d, 0x008d, 0x004d, 0x00fb,
			0x0003, 0x0053, 0x0013, 0x00c3, 0x0023, 0x0073, 0x0033, 0x00c7,
			0x000b, 0x0063, 0x0023, 0x00a7, 0x0003, 0x0083, 0x0043, 0x00e7,
			0x0007, 0x005b, 0x001b, 0x0097, 0x0043, 0x007b, 0x003b, 0x00d7,
			0x0013, 0x006b, 0x002b, 0x00b7, 0x000b, 0x008b, 0x004b, 0x00f7,
			0x0005, 0x0057, 0x0017, 000000, 0x0033, 0x0077, 0x0037, 0x00cf,
			0x000f, 0x0067, 0x0027, 0x00af, 0x0007, 0x0087, 0x0047, 0x00ef,
			0x0009, 0x005f, 0x001f, 0x009f, 0x0063, 0x007f, 0x003f, 0x00df,
			0x001b, 0x006f, 0x002f, 0x00bf, 0x000f, 0x008f, 0x004f, 0x00ff
	};

	unsigned char distcode_op[16] = {
			0x10, 0x17, 0x13, 0x1b, 0x11, 0x19, 0x15, 0x1d,
			0x10, 0x18, 0x14, 0x1c, 0x12, 0x1a, 0x16, 0x40
	};
#pragma HLS ARRAY_PARTITION variable=distcode_op complete dim=1
	unsigned char distcode_bits[32] = {
			0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
			0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
			0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
			0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05
	};
#pragma HLS ARRAY_PARTITION variable=distcode_bits complete dim=1
	unsigned short distcode_val[32] = {
			0x0001, 0x0101, 0x0011, 0x1001, 0x0005, 0x0401, 0x0041, 0x4001,
			0x0003, 0x0201, 0x0021, 0x2001, 0x0009, 0x0801, 0x0081, 000000,
			0x0002, 0x0181, 0x0019, 0x1801, 0x0007, 0x0601, 0x0061, 0x6001,
			0x0004, 0x0301, 0x0031, 0x3001, 0x000d, 0x0c01, 0x00c1, 000000
	};
#pragma HLS ARRAY_PARTITION variable=distcode_val complete dim=1

    ap_uint<16> in_addr;      /* local strm->next_in */
    ap_uint<1> firstBlk;
    ap_uint<1> lastBlk;
    ap_uint<32> hold;         /* local strm->hold */
    ap_uint<25> hold_shf1;
    ap_uint<18> hold_shf1_s1;
    ap_uint<11> hold_shf1_s2;
    ap_uint<16> hold_shf2;
    ap_uint<96> long_hold;
    ap_uint<7> bits;              /* local strm->bits */
    ap_uint<32> tmp_lendist;
    ap_uint<7> tmp_rshbits;
    //unsigned char bits_last;

    unsigned short len;               /* match length, unused bytes */
    unsigned short dist;              /* match distance */
	//unsigned mode;

	ap_uint<1> fir_rflag;
	ap_uint<1> sec_rflag;
	ap_uint<1> thi_rflag;
	ap_uint<1> firsec_rflag;
	ap_uint<1> firsecthi_rflag;
	ap_uint<1> endofblock;
	ap_uint<1> endofblock_reg;
	ap_uint<1> lenbase_flag;

	ap_uint<9> firLen_idx;
	ap_uint<5> firLen_idx_low;
	ap_uint<8> firLen_op;
	ap_uint<4> firLen_bits;
	unsigned short firLen_val;

	ap_uint<9> secLen_idx;
	ap_uint<5> secLen_idx_low;
	ap_uint<4> secLen_bits;
	ap_uint<8> secLen_val;

	ap_uint<9> thiLen_idx;
	ap_uint<5> thiLen_idx_low;
	ap_uint<4> thiLen_bits;
	ap_uint<8> thiLen_val;

	ap_uint<4> lenType;
	ap_uint<4> distType;
	ap_uint<4> firDist_idx_low;
	ap_uint<5> firDist_idx;
	ap_uint<8> firDist_op;
	ap_uint<4> firDist_bits;
	unsigned short firDist_val;

	ap_uint<5> firsecLen_bits;
	ap_uint<5> firsecthiLen_bits;
	ap_uint<4> len_exbits;
	ap_uint<4> dist_exbits;
	unsigned char shiftbits;
	//const unsigned rawflag = 0x01110119;
	ap_uint<1> rawflag[32] = {0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
			                  0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1};
#pragma HLS ARRAY_PARTITION variable=rawflag complete dim=1
 //0x01110119;
	bool finish = false;
	ap_uint<16> blkSize;

    in_addr = 0; //paramin[0];
    //last = avail_in + 4;// - 5);
    //long_hold = 0; //(ap_uint<64>)(paramin[8]); // << 32) + paramin[9];
    //bits = 0; //paramin[9];
	//mode = paramin[15];

    hold = *hold_p;
    long_hold = *long_hold_p;
    bits = *bits_p;
    blkSize = bramcodein[in_addr];
    in_addr++;
    firstBlk = blkIdx(0, 0);
    lastBlk = blkIdx(1, 1);
    endofblock_reg = 0;
    /* decode literals and length/distances until end-of-block or not enough
       input data or output space */
    if (firstBlk) {
		while (bits < 65) {
#pragma HLS PIPELINE II=1
			long_hold(bits+31, bits) = bramcodein[in_addr];
			in_addr++;
			bits += 32;
		}
		long_hold >>= 19;
		bits -= 19;
		hold = long_hold(31, 0); // & 0x0FFFFFFFF;
    }
	while (!finish) {
#pragma HLS PIPELINE II=2
		//bits_last = bits;
        if (bits < 63) {
        	//long_hold += ((ap_uint<96>)codein[in]) << bits;
        	long_hold(bits+31, bits) = bramcodein[in_addr];
        	in_addr++;
        	bits += 32;
    		if (in_addr*4 >= (blkSize+1)) {
    			if ((lastBlk) && (endofblock_reg)) {
    				break;
    			}
    			else {
    				finish = true;
    			}
    		}
        }
        shiftbits = 0;
        //hold = long_hold & 0x0FFFFFFFF;
        //firLen_idx = hold(8, 0); //(ap_uint<9>)hold & lmask;//
        //firLen_idx_low = hold(4, 0);
        firLen_idx = hold; //(ap_uint<9>)hold & lmask;//
        firLen_idx_low = hold;
        //firLen_op = lencode_op[firLen_idx];//hold & lmask];
        firLen_bits = lencode1_bits[firLen_idx_low];//hold & lmask];
        firLen_val = lencode1_val[firLen_idx];//hold & lmask];
        //fir_rflag = (rawflag & (1 << firLen_idx_low)) ? 0 : 1;
        fir_rflag = rawflag[firLen_idx_low];
        hold_shf1 = hold >> firLen_bits;
        //bits -= firLen_bits;
        //long_hold >>= firLen_bits;
        shiftbits += firLen_bits;

        //secLen_idx = hold_shf1(8, 0);
        //secLen_idx_low = hold_shf1(4, 0);
        secLen_idx = hold_shf1;
        secLen_idx_low = hold_shf1;
        secLen_bits = lencode2_bits[secLen_idx_low];
        secLen_val = lencode2_val[secLen_idx];
        //sec_rflag = (rawflag & (1 << secLen_idx_low)) ? 0 : 1;
        sec_rflag = rawflag[secLen_idx_low];
        firsec_rflag = fir_rflag & sec_rflag;
        hold_shf1_s1 = hold_shf1 >> secLen_bits;

        //thiLen_idx = hold_shf1_s1(8, 0);
        //thiLen_idx_low = hold_shf1_s1(4, 0);
        thiLen_idx = hold_shf1_s1;
        thiLen_idx_low = hold_shf1_s1;
        thiLen_bits = lencode3_bits[thiLen_idx_low];
        thiLen_val = lencode3_val[thiLen_idx];
        //thi_rflag = (rawflag & (1 << thiLen_idx_low)) ? 0 : 1;
        thi_rflag = rawflag[thiLen_idx_low];
        firsecthi_rflag = firsec_rflag & thi_rflag;

        //lenType = firLen_op(7, 4);
        if (firLen_idx(6, 0) == 0) {
        	lenType = 6;
        	endofblock = 1;
        	lenbase_flag = 0;
        }
        else if (firLen_idx(6, 0) == 99) {
        	lenType = 4;
        	endofblock = 0;
        	lenbase_flag = 0;
        }
        //else if (rawflag & (1 << firLen_idx_low)) {
        else if (!rawflag[firLen_idx_low]) {
        	lenType = 1;
        	endofblock = 0;
        	lenbase_flag = 1;
        }
        else {
        	lenType = 0;
        	endofblock = 0;
        	lenbase_flag = 0;
        }
        endofblock_reg = endofblock;
        len = firLen_val;
        //len_exbits = firLen_op(3, 0); // & 15;                           /* number of extra bits */
        if ((firLen_idx(2, 0) == 0)) {
        	len_exbits = lencode1_extbits[firLen_idx(6, 3)];
        }
        else if (firLen_idx(4, 0) == 3) {
        	len_exbits = lencode2_extbits[firLen_idx(7, 5)];
        }
        else if (firLen_idx(3, 0) == 4) {
        	len_exbits = lencode3_extbits[firLen_idx(6, 4)];
        }
        else {
        	len_exbits = 0;
        }
        if (lenbase_flag) {                     /* length base */
            if (len_exbits) {
                len += hold_shf1(5, 0) & ((1U << len_exbits) - 1);
                hold_shf1 >>= len_exbits;
                shiftbits += len_exbits;
            }
            firDist_idx = hold_shf1(4, 0); //(ap_uint<5>)hold & dmask;//hold(4+firLen_bits+len_exbits, firLen_bits+len_exbits);
            firDist_idx_low = hold_shf1(3, 0);
            firDist_op = distcode_op[firDist_idx_low];//hold & dmask];
            firDist_bits = 5; //distcode_bits[firDist_idx];//hold & dmask];
            firDist_val = distcode_val[firDist_idx];//hold & dmask];
            hold_shf2 = hold_shf1 >> firDist_bits;

            shiftbits += firDist_bits;
            distType = firDist_op(7, 4);
            dist = firDist_val;
            dist_exbits = firDist_op(3, 0); // & 15;                       /* number of extra bits */
            if (distType(0, 0)) {                      /* distance base */
                dist += hold_shf2(13, 0) & ((1U << dist_exbits) - 1);
                shiftbits += dist_exbits;
            }
            //else {
                //strm->msg = (char *)"invalid distance code";
                //mode = BAD;
            //    break;
            //}
        }

		firsecLen_bits = (ap_uint<5>)secLen_bits + firLen_bits;
		firsecthiLen_bits = (ap_uint<5>)thiLen_bits + firsecLen_bits; //(ap_uint<5>)secLen_bits + (ap_uint<5>)firLen_bits;
		//if (fir_rflag & sec_rflag & thi_rflag) {
		if (firsecthi_rflag) {
			//com_lendist[j] = (0x13 << 24) + ((thiLen_val & 0x0FF) << 16) + ((secLen_val & 0x0FF) << 8) + (firLen_val & 0x0FF);
			//j++;
			//long_hold >>= firsecthiLen_bits;
			//bits -= firsecthiLen_bits;
			tmp_lendist(31, 24) = 0x13;
			tmp_lendist(23, 16) = thiLen_val & 0x0FF;
			tmp_lendist(15, 8) = secLen_val & 0x0FF;
			tmp_lendist(7, 0) = firLen_val & 0x0FF;
			tmp_rshbits = firsecthiLen_bits;
			//if (!com_lendist.full()) {
			//	com_lendist.write(tmp_lendist);
			//}
		}
		//else if (fir_rflag & sec_rflag) {
		else if (firsec_rflag) {
		//if (fir_rflag & sec_rflag) {
			//com_lendist[j] = (0x12 << 24) + ((secLen_val & 0x0FF) << 8) + (firLen_val & 0x0FF);
			//j++;
			//long_hold >>= firsecLen_bits;
			//bits -= firsecLen_bits;
			tmp_lendist(31, 24) = 0x12;
			tmp_lendist(23, 16) = 0;
			tmp_lendist(15, 8) = secLen_val & 0x0FF;
			tmp_lendist(7, 0) = firLen_val & 0x0FF;
			tmp_rshbits = firsecLen_bits;
			//if (!com_lendist.full()) {
			//	com_lendist.write(tmp_lendist);
			//}
		}
		else if (fir_rflag) {
			//com_lendist[j] = (0x11 << 24) + (firLen_val & 0x0FF);
			//j++;
			//long_hold >>= firLen_bits;
			//bits -= firLen_bits;
			tmp_lendist(31, 24) = 0x11;
			tmp_lendist(23, 16) = 0;
			tmp_lendist(15, 8) = 0;
			tmp_lendist(7, 0) = firLen_val & 0x0FF;
			tmp_rshbits = firLen_bits;
			//if (!com_lendist.full()) {
			//	com_lendist.write(tmp_lendist);
			//}
		}
		else if (lenbase_flag) {
			//com_lendist[j] = (8 << 28) + (len << 16) + dist;
			//j++;
			//long_hold >>= shiftbits;
			//bits -= shiftbits;
			tmp_lendist(31, 28) = 8;
			tmp_lendist(27, 16) = len & 0x0FFF;
			tmp_lendist(15, 0) = dist;
			tmp_rshbits = shiftbits;
			//if (!com_lendist.full()) {
			//	com_lendist.write(tmp_lendist);
			//}
		}
		else if (endofblock) {
			//long_hold >>= firLen_bits + 3;
			//bits -= firLen_bits + 3;
			tmp_rshbits = firLen_bits + 3;
		}
		if (!endofblock) {
			com_lendist.write(tmp_lendist);
		}
		long_hold >>= tmp_rshbits;
		bits -= tmp_rshbits;
        hold = long_hold(31, 0); // & 0x0FFFFFFFF;

		//if (in*4 >= blkSize) {
		//	break;
		//}
    }
	if (lastBlk) {
		//if (!com_lendist.full()) {
			com_lendist.write(0xFFFFFFFF);
		//}
	}
    *hold_p = hold;
    *long_hold_p = long_hold;
    *bits_p = bits;
}

void load_block(volatile unsigned *codein, unsigned* blockMem, unsigned blockStaAddr, ap_uint<16> blockDatsize)
{

	blockMem[0] = blockDatsize;
	memcpy(&blockMem[1], (const void*)&codein[blockStaAddr], blockDatsize);
}

void data_parse(volatile unsigned *codein, unsigned avail_in, stream<unsigned>& com_lendist)
{
//#pragma HLS INLINE
	unsigned i=0;
	unsigned blockNums;
	unsigned blockStaAddr;
	ap_uint<2> blkIdx;
	unsigned blockMem1[BLOCK_DATA_SIZE];
	unsigned blockMem2[BLOCK_DATA_SIZE];
//#pragma HLS RESOURCE variable=blockMem core=RAM_2P_BRAM
	//unsigned blockMem_A[BLOCK_DATA_SIZE];
	//unsigned blockMem_B[BLOCK_DATA_SIZE];
	ap_uint<16> blockDatsize;
	//ap_uint<16> blockDatsize_reg;
	ap_uint<16> last_blockDatsize;
	ap_uint<32> totalinp;
    ap_uint<32> hold;
    ap_uint<96> long_hold;
    ap_uint<7> bits;

	hold = 0;
	long_hold = 0;
	bits = 0;
    totalinp = avail_in;
	if (totalinp(BLKSIZE_DEPTH-1, 0) != 0) {
		blockNums = totalinp(31, BLKSIZE_DEPTH) + 1;
	}
	else {
		blockNums = totalinp(31, BLKSIZE_DEPTH);
	}
	//blockDatsize = 1024;
	last_blockDatsize = totalinp(BLKSIZE_DEPTH-1, 0) + 4;
	blockStaAddr = 0;
data_parse_loop :
    for (i=0; i<blockNums+1; i++) {
		if (i == (blockNums - 1)) {
			if (last_blockDatsize == 0) {
				blockDatsize = 1 << BLKSIZE_DEPTH;
			}
			else {
				blockDatsize = last_blockDatsize;
			}
		}
		else {
			blockDatsize = 1 << BLKSIZE_DEPTH;
		}
		if (i == 1) {
			if (1 == blockNums) {
				blkIdx = 3;
			}
			else {
				blkIdx = 1;
			}
		}
		else if (i == blockNums) {
			blkIdx = 2;
		}
		else {
			blkIdx = 0;
		}
		blockStaAddr = i*((1 << BLKSIZE_DEPTH)/4);
		if (i%2 == 0) {
			if (i == 0) {
				load_block(codein, blockMem1, blockStaAddr, blockDatsize);
			}
			else if (i == blockNums) {
				inflate_decoder(blockMem2, com_lendist, blkIdx, &hold, &long_hold, &bits);
			}
			else {
				load_block(codein, blockMem1, blockStaAddr, blockDatsize);
				inflate_decoder(blockMem2, com_lendist, blkIdx, &hold, &long_hold, &bits);
			}
		}
		else {
			if (i == blockNums) {
				inflate_decoder(blockMem1, com_lendist, blkIdx, &hold, &long_hold, &bits);
			}
			else {
				load_block(codein, blockMem2, blockStaAddr, blockDatsize);
				inflate_decoder(blockMem1, com_lendist, blkIdx, &hold, &long_hold, &bits);
			}
		}
		//memcpy(blockMem, (const void*)&codein[blockStaAddr], blockDatsize);
		//inflate_decoder(blockMem, com_lendist, blkIdx, blockDatsize, &hold, &long_hold, &bits);
	}
}

void assemble_integer(stream<uint68>& decout, stream<uint66>& dec2upload)
{
	ap_uint<128> hold_out;
	ap_uint<4> bytes_out;
	uint68 tmpdec2assem;
	uint66 tmpdec2upload;
	ap_uint<4> tmpBnum;
	ap_uint<64> tmpInt2assem;
	unsigned decsyb_cnt; // test
	//unsigned i=0; // test

	decsyb_cnt = 0; // test
	hold_out = 0;
	bytes_out = 0;
	while(1) {
#pragma HLS PIPELINE II=1
		if (!decout.empty()) {
			tmpdec2assem = decout.read();
			tmpBnum = tmpdec2assem(67, 64);
			tmpInt2assem = tmpdec2assem(63, 0);
			decsyb_cnt++; // test
			if (tmpBnum == 0xF) {
				//out = tmpdec2assem(31, 0);
				break;
			}
			hold_out(bytes_out*8+63, bytes_out*8) = tmpInt2assem;
			bytes_out += tmpBnum;
			if (bytes_out >= 8) {
				//if (!dec2upload.full()) {
				tmpdec2upload(65, 64) = 0;
				tmpdec2upload(63, 0) = hold_out(63, 0);
					dec2upload.write(tmpdec2upload);
				//}
				//j++;
				//if (j == 13) {
				//	printf("dec2upload: Test point.\n");
				//}
				hold_out >>= 64;
				bytes_out -= 8;
			}
		}
	}
	if (bytes_out != 0) {
		tmpdec2upload(65, 64) = 0;
		tmpdec2upload(63, 0) = hold_out(63, 0);
		dec2upload.write(tmpdec2upload);
		decsyb_cnt++; // test
	}
	printf("decsyb_cnt = %d\n", decsyb_cnt);
	//if (!dec2upload.full()) {
	tmpdec2upload(65, 64) = 3;
	tmpdec2upload(63, 0) = tmpInt2assem;
	dec2upload.write(tmpdec2upload);
	//}
}

void gen_decdat(stream<unsigned>& com_lensta, stream<uint68>& decout)
{
	//unsigned short i;
    unsigned out;     /* local strm->next_out */
	char rtval;
	//unsigned dout_cnt = 0;
	//unsigned dout_cnt_reg = 0;
    //unsigned decout_buf1[16384];
    //unsigned decout_buf2[16384];
    //unsigned decStaAddr;
	uint64 decodeBuf1[4320];
#pragma HLS RESOURCE variable=decodeBuf1 core=RAM_2P_BRAM
//	uint64 decodeBuf2[4320];
//#pragma HLS RESOURCE variable=decodeBuf2 core=RAM_2P_BRAM

    //decStaAddr = 0;
    out = 0;
	rtval = 0;
	//for (i=0; i<16384000; i++) {
	while (1) {
		//if (i%2 == 0) {
			//rtval = copy_data(com_lensta, decodeBuf1, decodeBuf2, decout, &out);
			rtval = copy_data(com_lensta, decodeBuf1, decout, &out);
		//}
		//else {
		//	rtval = copy_data(com_lensta, decodeBuf2, decodeBuf1, decout, &out);
		//}
		if (rtval == 2) {
			break;
		}
	}
	//return out;
}

//char fill_resulBuf(stream<uint66>& results, uint64 *resultsBuf, unsigned short *locAddr, unsigned* out_p)
char fill_resulBuf(stream<uint66>& results, uint64 *resultsBuf, unsigned* out_p)
{
	char rtval;
	uint66 endFlag=0;
	unsigned short locAddr_t;
	unsigned out;

	out = *out_p;
	//locAddr_t = *locAddr;
	locAddr_t = 1;
	while(1) {
#pragma HLS PIPELINE II=1
		if (!results.empty()) {
			results.read(endFlag);
			//if (endFlag == 0xFFFFFFFF) {
			if (endFlag(65, 64) == 3) {
				//*locAddr = locAddr_t;
				resultsBuf[0] = locAddr_t - 1;
				//if (locAddr_t != 0) {
					rtval = 1;
				//}
				//else {
				//	rtval = 2;
				//}
				out = endFlag(31, 0);
				break;
			}
			else {
				resultsBuf[locAddr_t] = endFlag(63, 0);
				locAddr_t = locAddr_t + 1;
			    if (locAddr_t >= 1000) {
					//*locAddr = locAddr_t;
			    	resultsBuf[0] = locAddr_t - 1;
					rtval = 0;
					break;
			    }
			}
		}
/*		else {
			if (endFlag(33, 32) == 3) {
				*locAddr = locAddr_t;
				if (locAddr_t != 0) {
					rtval = 1;
				}
				else {
					rtval = 2;
				}
				break;
			}
		}*/
	}
	*out_p = out;
	return rtval;
}

void upload_block(uint64* resultsBuf, volatile uint64 *output_a, unsigned* outAddr_p)
{
	unsigned short locAddr;
	unsigned outAddr;

	outAddr = *outAddr_p;
	locAddr = resultsBuf[0];
	memcpy((void*)&output_a[outAddr], &resultsBuf[1], locAddr*8);
	outAddr = outAddr + locAddr;
	*outAddr_p = outAddr;
}

void upload_results(stream<uint66>& results, volatile uint64 *output_a)
{
	char flag_over;
	unsigned i;
	unsigned short locAddr;
	unsigned short locAddr_reg;
	unsigned out;
	//unsigned outbuf[8];
	unsigned outAddr;
//	uint64 resultsBuf[1024];
//#pragma HLS RESOURCE variable=resultsBuf core=RAM_2P_BRAM
	uint64 resultsBuf1[1024];
//#pragma HLS RESOURCE variable=resultsBuf1 core=RAM_2P_BRAM
	uint64 resultsBuf2[1024];
//#pragma HLS RESOURCE variable=resultsBuf2 core=RAM_2P_BRAM

	out = 0;
	//outbuf[0] = 0;
	outAddr = 1;
	//while(1) {
	//	locAddr = 0;
	//	flag_over = fill_resulBuf(results, resultsBuf, &locAddr, &out);
	//	if (flag_over == 2) {
	//		break;
	//	}
	//	else if (flag_over == 1) {
	//		memcpy((void*)&output_a[outAddr], resultsBuf, locAddr*8);
	//		break;
	//	}
	//	else if (flag_over == 0) {
	//		memcpy((void*)&output_a[outAddr], resultsBuf, locAddr*8);
	//		outAddr = outAddr + locAddr;
	//	}
	//}
	for (i=0; i<2281701376; i++) {
		if (i%2 == 0) {
			if (i == 0) {
				flag_over = fill_resulBuf(results, resultsBuf1, &out);
			}
			else {
				if (flag_over == 1) {
					upload_block(resultsBuf2, output_a, &outAddr);
					break;
				}
				else {
					flag_over = fill_resulBuf(results, resultsBuf1, &out);
					upload_block(resultsBuf2, output_a, &outAddr);
				}
			}
		}
		else {
			if (flag_over == 1) {
				upload_block(resultsBuf1, output_a, &outAddr);
				break;
			}
			else {
				flag_over = fill_resulBuf(results, resultsBuf2, &out);
				upload_block(resultsBuf1, output_a, &outAddr);
			}
		}
	}
	//outbuf[0] = out;
	//memcpy((void*)&output_a[0], outbuf, 8);
	output_a[0] = out;
}

extern "C" {
void inflate_fast_hw(volatile unsigned* codein, volatile uint64* decodeout, unsigned avail_in)
{
#pragma HLS INTERFACE m_axi port=codein offset=slave bundle=gmem1 depth=1024000000
#pragma HLS INTERFACE m_axi port=decodeout offset=slave bundle=gmem2 depth=204800000
#pragma HLS INTERFACE s_axilite port=codein bundle=control
#pragma HLS INTERFACE s_axilite port=decodeout bundle=control
#pragma HLS INTERFACE s_axilite port=avail_in bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

	//unsigned cld_cnt = 0;
	unsigned codeLength;
	//unsigned out;

	stream<unsigned> com_lendist("com_lendist");
#pragma HLS STREAM variable=com_lendist depth=4096
	//unsigned com_lendist[30720];
	stream<unsigned> com_lensta("com_lensta");
#pragma HLS STREAM variable=com_lensta depth=4096
	//unsigned com_lensta[16384];
	stream<uint68> decout("decout2assemb");
#pragma HLS STREAM variable=decout depth=2048
	stream<uint66> dec2upload("dec2upload");
#pragma HLS STREAM variable=dec2upload depth=2048

    //out = 0;
    codeLength = avail_in;
#pragma HLS DATAFLOW
    //memcpy(codein_buf, (const void*)codein, codeLength);
	//inflate_decoder(codein_buf, com_lendist, avail_in);
	data_parse(codein, codeLength, com_lendist);
	calc_lensta(com_lendist, com_lensta);
	gen_decdat(com_lensta, decout);
	assemble_integer(decout, dec2upload);
	upload_results(dec2upload, decodeout);
	//memcpy(decodeout, decodeout_buf, out);
	//return 0;
}
}
