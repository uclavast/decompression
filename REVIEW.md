# Accelerator Code Review: Decompression

## Reviewers
Peng Wei, peng.wei.prc@cs.ucla.edu

## Problems
0. Is the data_parse step fully pipelined (even though the child procedure calls reach II=1)? I was wondering if it would be possible that the first step might become the bottleneck of the pipeline.
0. Is the upload_results step fully pipelined? It might also be possible to serve as a bottleneck of the whole design.

## Coding Style
0. Better not use any tabs, otherwise will break the indentation, e.g., line 1448
0. Line 1135-1142: totalinp is used as a verilog wire, which might be not that readable by C programmers. I was wondering if there could be any C-like approaches to achieve the same performance.
0. Line 1142-1156: last_blockDatsize might be considered to initialize with blockNums, i.e., if (totalinp(BLKSIZE_DEPTH-1,0)!=0) {blockNum = ...; last_blockDatsize = ...}. Then, we do not need the if condition at Line 1147.

## Suggestions
The design has two level of pipelining. The first level is the pipeline of the five steps, and the second is the pipeline inside each step. The pipeline performance will be bounded by the longest-latency step. Since there might be some steps which might not be fully pipelined, the pipeline performance would desearve a re-examination.

